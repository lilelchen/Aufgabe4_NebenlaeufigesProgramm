/**
 * Die Klasse Speicher sichert eine Zahl vom Zaehler in den Speicher. 
 * 
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 */

public class Speicher implements SpeicherIf {

	private int wert;
	private boolean hatWert = false;
	
	/**
	 * @return result
	 * @throws InterruptedException
	 */
	
	@Override	
	public int getWert() throws InterruptedException {
		int result;
		synchronized (this){
			result = wert;
			
			// Der getWert sagt allen wartenden Threads, dass er seine Ausfuehrung abgeschlossen hat.
			this.notifyAll();
		}	
		return result;		
	}
	
	
	@Override
	public void setWert(int wert) throws InterruptedException {
		synchronized (this){
			
		this.wert = wert;
		
		hatWert = true;
		
		this.notifyAll();
		this.wait();
		}
	}

	@Override
	public boolean isHatWert() {
		return hatWert;
	}
}
